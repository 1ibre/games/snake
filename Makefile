CC=g++
FLAGS=-Wall -Wextra -Werror -pedantic
LIBS=-lm -lGL -lGLU -lglut
SRCS=${wildcard ./src/*.cpp ./src/*/*.cpp ./src/*/*/*.cpp}
NAME=Snake


all: build

build:
	@mkdir -p ./bin
	$(CC) $(FLAGS) $(SRCS) -o ./bin/$(NAME) $(LIBS)

debug:
	@mkdir -p ./bin/debug
	$(CC) -g $(FLAGS) $(SRCS) -o ./bin/debug/$(NAME) $(LIBS)
	gdb ./bin/debug/$(NAME)

run: build
	./bin/$(NAME)

install:
	@echo "Not implemented yet"

clean:
	rm -rf ./bin/*
#include <GL/gl.h>


void rect(unsigned int x1, unsigned int y2, unsigned int s1, unsigned int s2)
{
    glBegin(GL_QUADS);
    glVertex2i(x1, y2);
    glVertex2i(x1 + s1, y2);
    glVertex2i(x1 + s1, y2 + s2);
    glVertex2i(x1, y2 + s2);
    glEnd();
}

void point(unsigned int x, unsigned int y)
{
    glBegin(GL_POINTS);
    glVertex2i(x, y);
    glEnd();
}

void fill(float red, float green, float blue)
{
    glColor3f(red, green, blue);
}
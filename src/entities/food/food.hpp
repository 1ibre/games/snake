#pragma once

#include <random>

#include "../../game.hpp"
#include "../../utils/graphics.hpp"

class Food
{
public:
    Food();
    Food(int x_, int y_);
    static Food random();
    void show();
    int get_x();
    int get_y();

private:
    int x;
    int y;
    unsigned int size;
};
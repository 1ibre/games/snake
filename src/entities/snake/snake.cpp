#include "./snake.hpp"
#include <iostream>

Snake::Snake()
{
    *this = Snake(0, 0);
}

Snake::Snake(unsigned int x_, unsigned int y_)
{
    x = x_;
    y = y_;
    size = 10;
    // default direction is right
    direction[0] = Snake::RIGHT[0];
    direction[1] = Snake::RIGHT[1];
    length = 1;
    alive = true;
    speed = 2;
}

Snake::~Snake()
{
    length = 0;
    tail.clear();
}

void Snake::update()
{
    if (alive)
    {
        move();
        if (checkColisions())
        {
            die();
        }
    }
    draw();
}

void Snake::draw()
{
    fill(1, 1, 1);
    for (auto &part : tail)
    {
        rect(std::get<0>(part), std::get<1>(part), size, size);
    }
    rect(x, y, size, size);
}

void Snake::move()
{
    tail.push_front(std::make_tuple(x, y));
    x += direction[0] * speed;
    y += direction[1] * speed;
    tail.pop_back();
}

bool Snake::checkColisions()
{
    return x < -WIDTH / 2 || x + (int)size > WIDTH / 2 || y - (int)size < -HEIGHT / 2 || y > HEIGHT / 2;
}

void Snake::die()
{
    alive = false;
    std::cout << "You died!" << std::endl;
}

bool Snake::is_eating(Food food)
{
    int food_x = food.get_x();
    int food_y = food.get_y();
    bool on_food = x - (int)size < food_x && x + (int)size > food_x && y - (int)size < food_y && food_y < y + (int)size;
    if (on_food)
    {
        grow();
    }
    return on_food;
}

void Snake::grow()
{
    length++;
    tail.push_back(std::make_tuple(x, y));
    x += direction[0] * speed;
    y += direction[1] * speed;
}

void Snake::setDirection(const int *heading)
{
    direction[0] = heading[0];
    direction[1] = heading[1];
}
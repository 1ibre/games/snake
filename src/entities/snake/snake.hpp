#pragma once

#include <deque>
#include <tuple>
#include <string>

#include "../../utils/graphics.hpp"
#include "../../game.hpp"
#include "../food/food.hpp"

class Snake
{
public:
    Snake();
    Snake(unsigned int x, unsigned int y);
    ~Snake();

    constexpr static int UP[2] = {0, 1};
    constexpr static int DOWN[2] = {0, -1};
    constexpr static int LEFT[2] = {-1, 0};
    constexpr static int RIGHT[2] = {1, 0};

    void draw();
    void update();
    void move();
    void grow();
    void die();
    bool checkColisions();
    void setDirection(const int *heading);
    bool is_eating(Food food);
    
private:
    bool alive;
    int x;
    int y;
    unsigned int size;
    int direction[2];
    unsigned int length;
    unsigned int speed;
    std::deque<std::tuple<int, int>> tail;
};
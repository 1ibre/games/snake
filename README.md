# Snake

Basic OpenGL Snake game and Neural Network / Genetic Algorithm Snake AI.

***
<!-- ## Visuals -->

## Installation

```bash
git clone https://framagit.org/1ibre/games/snake.git
cd snake
make
```

## Usage

```bash
make run
# or
./bin/snake
```

## Support

For any questions, feature requests or bug reports, please visit the issue tracker on [the gitlab repository](https://framagit.org/1ibre/games/snake/issues).

## Roadmap

 * [x] Snake
 * [ ] Snake AI

## Contributing

Pull requests are welcome.

## Authors and acknowledgment

 - [Samuel ORTION](https://samuel.ortion.fr/)
## License

This project is available under the GNU General Public License v3.0 or later.

## Project status

This project is currently in development.
